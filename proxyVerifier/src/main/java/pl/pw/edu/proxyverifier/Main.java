package pl.pw.edu.proxyverifier;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class Main {

    /**
     * Zawartość bajtowa pliku wejściowego, który podpisujemy
     */
    private byte[] fileData;
    private String signatureFile;
    private String publicKey;
    private final BigInteger p, g, y, sp, r, e, eprim;

    public Main(String[] args) {

        try {
            fileData = getFile(args[2]);
        } catch (IOException ex) {
            System.out.println("Nie udało się otworzyć pliku wejściowego. Spróbuj ponownie!");
            System.exit(1);
        }

        try {
            signatureFile = new String(getFile(args[1]), StandardCharsets.UTF_8);
        } catch (IOException ex) {
            System.out.println("Nie udało się otworzyć sygnatury. Spróbuj ponownie!");
            System.exit(1);
        }

        try {
            publicKey = new String(getFile(args[0]), StandardCharsets.UTF_8);
        } catch (IOException ex) {
            System.out.println("Nie udało się otworzyć klucza publicznego. Spróbuj ponownie!");
            System.exit(1);
        }
        String tmp[] = publicKey.split("#");

        p = new BigInteger(tmp[0], 16);
        g = new BigInteger(tmp[1], 16);
        y = new BigInteger(tmp[3], 16);

        String tmp2[] = signatureFile.split("#");

        sp = new BigInteger(tmp2[0], 16);
        e = new BigInteger(tmp2[1], 16);
        r = new BigInteger(tmp2[2], 16);

        eprim = genEprim(fileData, genValue(g, sp, y, r, e, p));

        System.out.println("e   = " + e);
        System.out.println("e'  = " + eprim);
        
        if(!e.equals(eprim)) {
            System.out.println("Weryfikacja NIE POWIODŁA SIĘ!");
        } else {
            System.out.println("Podpis jest wiarygodny.");
        }

    }

    public BigInteger genValue(BigInteger g, BigInteger sp, BigInteger y, BigInteger r, BigInteger e, BigInteger p) {
        return g.modPow(sp, p).multiply(y.modPow(e.negate(), p)).multiply(r.modPow(r.multiply(e.negate()), p)).mod(p);
    }

    public String bytesToString(byte[] _bytes) {
        String file_string = "";
        for (int i = 0; i < _bytes.length; i++) {
            file_string += String.format("%8s", Integer.toBinaryString(_bytes[i] & 0xFF)).replace(' ', '0');
        }
        return file_string;
    }

    public BigInteger genEprim(byte[] m, BigInteger value) {

        byte[] bytes = value.toByteArray();
        byte[] c = new byte[m.length + bytes.length];
        System.arraycopy(m, 0, c, 0, m.length);
        System.arraycopy(bytes, 0, c, m.length, bytes.length);

//        System.out.println("value: " + bytesToString(bytes));
//        System.out.println("m: " + bytesToString(m));
//        System.out.println("c: " + bytesToString(c));

        MessageDigest mda = null;
        try {
            mda = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException ex) {
            System.out.println("Algorytm nie został znaleziony!");
            System.exit(1);
        }

        byte[] coded = mda.digest(c);
        return new BigInteger(coded);
    }

    public byte[] getFile(String filePath) throws IOException {
        Path file = Paths.get(filePath);
        if (file.toFile().exists()) {
            return Files.readAllBytes(file);
        } else {
            return null;
        }
    }

    public static void main(String[] args) {
        if (args.length == 3) {
            Main main = new Main(args);
        } else {
            System.out.println("Poprawna składnia: java -jar proxyVerifier.jar kluczPublicznySzefa SygnaturaPliku PlikDoWeryfikacji");
        }
    }

}
