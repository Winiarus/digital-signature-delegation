package pl.pw.edu.proxykeygen;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Random;

public final class Main {

    /**
     * Wartości pobrane od mocodawcy
     */
    private final BigInteger q, x, p, g, y;

    /**
     * Losowa liczba z przedziału (1, g)
     */
    private final BigInteger k;

    /**
     * r = g^k mod p
     */
    private final BigInteger r;

    /**
     * s = (x + kr) mod q // Czy ten nawias jest OK???
     */
    private final BigInteger s;

    private String bossPrivateKey = null;
    private String bossPublicKey = null;

    private static final BigInteger TWO = BigInteger.ONE.add(BigInteger.ONE);

    public Main(String[] args) {
        try {
            bossPrivateKey = getFile(args[0]);
        } catch (IOException ex) {
            System.out.println("Nie udało się otworzyć klucza prywatnego. Spróbuj ponownie!");
            System.exit(1);
        }
        
        try {
            bossPublicKey = getFile(args[1]);
        } catch (IOException ex) {
            System.out.println("Nie udało się otworzyć klucza publicznego. Spróbuj ponownie!");
            System.exit(1);
        }
        
        x = new BigInteger(bossPrivateKey, 16);

        String tmp[] = bossPublicKey.split("#");
        
        p = new BigInteger(tmp[0], 16);
        g = new BigInteger(tmp[1], 16);
        q = new BigInteger(tmp[2], 16);
        y = new BigInteger(tmp[3], 16);

        System.out.println("p = " + p);
        System.out.println("g = " + g);
        System.out.println("q = " + q);
        System.out.println("y = " + y);
        System.out.println("x = " + x);

        k = genK(q); // OK
        System.out.println("k = " + k);
        r = genR(g, k, p); // OK
        System.out.println("r = " + r);
        s = genS(x, k, r, q); // OK
        System.out.println("s = " + s);

        BigInteger L = g.modPow(s, p);
        BigInteger R = y.mod(p).multiply(r.modPow(r, p)).mod(p);

        if(!L.equals(R)) {
            System.out.println("Weryfikacja wygenerowanego klucza proxy nie powiodła się!");
        }
        
        StringBuilder sb = new StringBuilder();
        sb.append(r.toString(16)).append("#").append(s.toString(16));
        try {
            createFile(sb.toString(), "proxyKey.key");
        } catch (IOException ex) {
            System.out.println("Zapis klucza Proxy nie powiódł się!");
            System.exit(1);
        }
    }
    
    public String getFile(String filePath) throws IOException {
        Path file = Paths.get(filePath);
        if (file.toFile().exists()) {
            byte[] encoded = Files.readAllBytes(file);
            return new String(encoded, StandardCharsets.UTF_8);
        } else {
            return null;
        }
    }

    public void createFile(String data, String fileName) throws IOException {
        File file = new File(fileName);
        Files.write(file.toPath(), data.getBytes());
    }

    public BigInteger genR(BigInteger g, BigInteger k, BigInteger p) {
        return g.modPow(k, p);
    }

    public BigInteger genS(BigInteger x, BigInteger k, BigInteger r, BigInteger q) {
        return x.add(k.multiply(r)).mod(q);
    }

    public BigInteger genK(BigInteger n) {
        Random rand = new Random();
        BigInteger result = new BigInteger(n.bitLength(), rand);
        while (result.compareTo(n.subtract(BigInteger.ONE)) == 1 || result.compareTo(TWO) == -1) {
                result = new BigInteger(n.bitLength(), rand);
        }
        return result;
    }

    public static void main(String[] args) {
        if (args.length == 2) {
            Main main = new Main(args);
        } else {
            System.out.println("Poprawna składnia: java -jar proxyKeygen.jar kluczPrywatnySzefa kluczPublicznySzefa");
        }
    }

}
