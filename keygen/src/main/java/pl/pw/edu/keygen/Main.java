package pl.pw.edu.keygen;

import java.io.File;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import pl.pw.edu.keygen.AKS.AKS;
import java.io.IOException;
import java.nio.file.*;

public final class Main {

    /**
     * Duża liczba pierwsza
     */
    private final BigInteger p;

    /**
     * Największy czynnik pierwszy liczby (p - 1)
     */
    private final BigInteger q;

    /**
     * Element grupy cyklicznej Z*p rzędu q
     */
    private final BigInteger g;

    /**
     * Liczba losowa z przedziału od 1 do p
     */
    private final BigInteger x;

    /**
     * g^x mod p
     */
    private final BigInteger y;
    
    static Map<BigInteger, List<BigInteger>> factors = new HashMap<>();
    private static final BigInteger TWO = BigInteger.ONE.add(BigInteger.ONE);

    public Main()  {
        p = genP(20);
        System.out.println("p = " + p);
        q = genQ(p);
        System.out.println("q = " + q);
        g = genG(p, q);
        System.out.println("g = " + g);
        x = genX(p);
        System.out.println("x = " + x);
        y = genY(g, x, p);
        System.out.println("y = " + y);
          
        StringBuilder publicKey = new StringBuilder();
        publicKey.append(p.toString(16)).append("#").append(g.toString(16)).append("#").append(q.toString(16)).append("#").append(y.toString(16));
        try {
            createFile(publicKey.toString(), "public.key");
        } catch (IOException ex) {
            System.out.println("Zapisz klucza publicznego nie powiódł się!");
        }
        
        try {
            createFile(x.toString(16), "private.key");
        } catch (IOException ex) {
            System.out.println("Zapisz klucza prywatnego nie powiódł się!");
        }
    }
              
    public void createFile(String data, String fileName) throws IOException {
        File file = new File(fileName);
        Files.write(file.toPath(), data.getBytes());
    }
    
    public BigInteger genY(BigInteger g, BigInteger x, BigInteger p) {
        return g.modPow(x, p);
    }
    
    public BigInteger genX(BigInteger n) {
        Random rand = new Random();
        BigInteger result = new BigInteger(n.bitLength(), rand);
        while (result.compareTo(n.subtract(BigInteger.ONE)) == 1 || result.compareTo(TWO) == -1) {
                result = new BigInteger(n.bitLength(), rand);
        }
        return result;
    }

    public BigInteger genG(BigInteger p, BigInteger q) {
        BigInteger num = BigInteger.ONE;
        boolean tmp = false;
        while (!tmp || num.equals(p.subtract(BigInteger.ONE))) {
            num = num.add(BigInteger.ONE);
            BigInteger math = num.modPow(q, p);
            if (math.equals(BigInteger.ONE)) {
                tmp = true;
            }
        }
        return num;
    }

    public BigInteger genQ(BigInteger p) {
        List<BigInteger> primeFactors = factors(p.subtract(new BigInteger("1")), false);
        return primeFactors.get(primeFactors.size() - 1); // wybieramy ostatnią  - najwiękzy czynnik (MOŻNA ZMIENIĆ!)
    }

    public BigInteger genP(int length) {
        BigInteger rand = null;
        boolean isPrime = false;

        while (!isPrime) {
            rand = new BigInteger(length, 100, new Random());
            AKS primeTest = new AKS(rand);
            isPrime = primeTest.isPrime();
        }
        return rand;
    }

    public static List<BigInteger> factors(BigInteger n, boolean duplicates) {
        // Have we done this one before?
        List<BigInteger> f = factors.get(n);
        if (f == null) {
            // Start empty.
            f = new ArrayList<>();
            // Check for duplicates.
            BigInteger last = BigInteger.ZERO;
            // Limit the range as far as possible.
            for (BigInteger i = TWO; i.compareTo(n.divide(i)) <= 0; i = i.add(BigInteger.ONE)) {
                // Can have multiple copies of the same factor.
                while (n.mod(i).equals(BigInteger.ZERO)) {
                    if (duplicates || !i.equals(last)) {
                        f.add(i);
                        last = i;
                    }
                    // Remove that factor.
                    n = n.divide(i);
                }
            }
            if (n.compareTo(BigInteger.ONE) > 0) {
                // Could be a residue.
                if (duplicates || n != last) {
                    f.add(n);
                }
            }
            // Memoize.
            factors.put(n, f);
        }
        return f;
    }

    public static void main(String[] args) {
        Main main = new Main();
    }
}
