package pl.pw.edu.proxysigner;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

public final class Main {

    /**
     * Zawartość bajtowa pliku wejściowego, który podpisujemy
     */
    private byte[] fileData;

    /**
     * Klucz publiczny
     */
    private final BigInteger q, p, g;

    /**
     * Klucz Proxy
     */
    private final BigInteger s, r;
    private static final BigInteger TWO = BigInteger.ONE.add(BigInteger.ONE);

    private final BigInteger l, rp, sp, e;

    public Main(String[] args) {

        try {
            fileData = getFile(args[2]);
        } catch (IOException ex) {
            System.out.println("Nie udało się otworzyć pliku wejściowego. Spróbuj ponownie!");
            System.exit(1);
        }

        String proxyKey = null;
        try {
            proxyKey = new String(getFile(args[0]), StandardCharsets.UTF_8);
        } catch (IOException ex) {
            System.out.println("Nie udało się otworzyć klucza Proxy. Spróbuj ponownie!");
            System.exit(1);
        }

        String tmp[] = proxyKey.split("#");
        r = new BigInteger(tmp[0], 16);
        s = new BigInteger(tmp[1], 16);

        String publicKey = null;
        try {
            publicKey = new String(getFile(args[1]), StandardCharsets.UTF_8);
        } catch (IOException ex) {
            System.out.println("Nie udało się otworzyć klucza publicznego. Spróbuj ponownie!");
            System.exit(1);
        }

        String tmp2[] = publicKey.split("#");

        p = new BigInteger(tmp2[0], 16);
        g = new BigInteger(tmp2[1], 16);
        q = new BigInteger(tmp2[2], 16);

        System.out.println("p = " + p);
        System.out.println("g = " + g);
        System.out.println("q = " + q);

        l = genL(q);
        System.out.println("l = " + l);

        rp = genRP(g, l, p);
        System.out.println("rp = " + rp);

        e = genE(fileData, rp);
        System.out.println("e = " + e);
        System.out.println("e(hex) = " + e.toString(16));

        sp = genSP(s, e, l, q);
        System.out.println("sp = " + sp);
        System.out.println("sp(hex) = " + sp.toString(16));
        
        System.out.println("r(hex) = " + r.toString(16));
        
        StringBuilder sb = new StringBuilder();
        sb.append(sp.toString(16)).append("#").append(e.toString(16)).append("#").append(r.toString(16));
        try {
            createFile(sb.toString(), "message.sign");
        } catch (IOException ex) {
            System.out.println("Zapis sygnatury nie powiódł się!");
            System.exit(1);
        }
    }

    public BigInteger genRP(BigInteger g, BigInteger l, BigInteger p) {
        return g.modPow(l, p);
    }

    public BigInteger genSP(BigInteger s, BigInteger e, BigInteger l, BigInteger q) {
        return l.add(s.multiply(e)).mod(q);
        //return l.subtract(s.multiply(e)).mod(q);
    }

    public BigInteger genE(byte[] m, BigInteger rp) {
        byte[] rpByte = rp.toByteArray();
        byte[] c = new byte[m.length + rpByte.length];
        System.arraycopy(m, 0, c, 0, m.length);
        System.arraycopy(rpByte, 0, c, m.length, rpByte.length);

        System.out.println("rpByte: " + bytesToString(rpByte));
        System.out.println("m: " + bytesToString(m));
        System.out.println("c: " + bytesToString(c));
        
        MessageDigest mda = null;
        try {
            mda = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException ex) {
            System.out.println("Algorytm nie został znaleziony!");
            System.exit(1);
        }
        
        System.out.println(new BigInteger(c));
        byte[] coded = mda.digest(c);
        return new BigInteger(coded);
    }

    public String bytesToString(byte[] _bytes) {
        String file_string = "";
        for (int i = 0; i < _bytes.length; i++) { 
            file_string += String.format("%8s", Integer.toBinaryString(_bytes[i] & 0xFF)).replace(' ', '0');
        }
        return file_string;
    }

    public BigInteger genL(BigInteger n) {
        Random rand = new Random();
        BigInteger result = new BigInteger(n.bitLength(), rand);
        while (result.compareTo(n.subtract(TWO)) == 1 || result.compareTo(TWO) == -1) {
            result = new BigInteger(n.bitLength(), rand);
        }
        return result;
    }

    public byte[] getFile(String filePath) throws IOException {
        Path file = Paths.get(filePath);
        if (file.toFile().exists()) {
            return Files.readAllBytes(file);
        } else {
            return null;
        }
    }

    public void createFile(String data, String fileName) throws IOException {
        File file = new File(fileName);
        Files.write(file.toPath(), data.getBytes());
    }

    public static void main(String[] args) {
        if (args.length == 3) {
            Main main = new Main(args);
        } else {
            System.out.println("Poprawna składnia: java -jar proxySigner.jar kluczProxy kluczPubliczny PlikDoPodpisu");
        }
    }

}
